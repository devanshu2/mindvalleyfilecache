//
//  DataCache.m
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "DataCache.h"

@interface DataCache()

@property (atomic, strong) NSMutableArray *cacheData;

@end

@implementation DataCache

- (instancetype)init{
    self = [super init];
    if (self) {
        _cacheData = [NSMutableArray new];
    }
    return self;
}

+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    static DataCache *theInstance = nil;
    dispatch_once(&onceToken, ^{
        theInstance = [[DataCache alloc] init];
    });
    return theInstance;
}

- (BOOL)isValidCacheInsertWithBytes:(NSInteger)length{
    return (length <= CACHE_SIZE);
}

- (NSData *)cacheDataForKey:(NSString *)key{
    @synchronized (_cacheData) {
        __block NSMutableDictionary *cacheElement = nil;
        __block NSInteger foundIndex = -1;
        [_cacheData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[obj objectForKey:KEY_CACHE_URL] isEqualToString:key]) {
                foundIndex = idx;
                cacheElement = [obj mutableCopy];
                *stop = YES;
            }
        }];
        if (cacheElement) {
            [_cacheData removeObjectAtIndex:foundIndex];
            [_cacheData insertObject:cacheElement atIndex:0];
            return [cacheElement objectForKey:KEY_CACHE_DATA];
        }
        else{
            return nil;
        }
    }
}

- (BOOL)setCacheData:(NSData *)data ForKey:(NSString *)key{
    @synchronized (_cacheData) {
        if (![self isValidCacheInsertWithBytes:data.length]) {
            return NO;
        }
        __block NSInteger newLength = data.length + [NSKeyedArchiver archivedDataWithRootObject:_cacheData].length;
        if (![self isValidCacheInsertWithBytes:newLength]) {
            __block NSMutableIndexSet *removeIndexes = [NSMutableIndexSet new];
            __weak typeof(self) weakSelf = self;
            [_cacheData enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSData *cachedData = [obj objectForKey:KEY_CACHE_DATA];
                newLength -= cachedData.length;
                [removeIndexes addIndex:idx];
                if ([weakSelf isValidCacheInsertWithBytes:newLength]) {
                    *stop = YES;
                }
            }];
            [_cacheData removeObjectsAtIndexes:removeIndexes];
        }
        [_cacheData insertObject:@{KEY_CACHE_DATA:data, KEY_CACHE_URL:key} atIndex:0];
        return YES;
    }
}

/*
 
 @synchronized (_cacheData) {
 NSInteger cacheLength = 0;
 for (NSDictionary *data in _cacheData) {
 NSData *cacheElement = [data objectForKey:KEY_CACHE_DATA];
 cacheLength += cacheElement.length;
 }
 if (<#condition#>) {
 <#statements#>
 }
 }
 
 */

@end
