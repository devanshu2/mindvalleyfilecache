//
//  CacheFileDownloadOperation.m
//  CacheApp
//
//  Created by Devanshu Saini on 03/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "CacheFileDownloadOperation.h"

@interface CacheFileDownloadOperation()<NSURLSessionDelegate>

@property (atomic, assign)      BOOL            _executing;
@property (atomic, assign)      BOOL            _finished;
@property (nonatomic, strong)   NSURLSession    *session;
@property (nonatomic, copy)     NSURLRequest    *request;
@property (nonatomic, strong)   NSMutableArray  *completionBlocks;
@property (nonatomic, assign)   BOOL            downloadSizeCheckDone;

@end

@implementation CacheFileDownloadOperation

- (instancetype)initWithURLRequest:(NSURLRequest *)request andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock{
    self = [super init];
    if (self) {
        self.request = [request copy];
        self.name = request.URL.absoluteString;
        self.completionBlocks = [NSMutableArray arrayWithObject:[completionBlock copy]];
        self.downloadSizeCheckDone = NO;
    }
    return self;
}

- (void)addCompletionHandler:(CacheFileDownloadOperationCompletionBlock)completionBlock{
    [self.completionBlocks addObject:[completionBlock copy]];
}

- (void) start;{
    if ([self isCancelled]){
        // Move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        self._finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    self._executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
}

- (void) main;{
    if ([self isCancelled]) {
        return;
    }
    @autoreleasepool {
        __weak typeof(self) weakSelf = self;
        NSURLSession *downloadSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSURLSessionDownloadTask *downloadtask = [downloadSession downloadTaskWithRequest:self.request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSData *fileData = nil;
            if (!error) {
                fileData = [NSData dataWithContentsOfURL:location];
                BOOL fileValidToCache = [[DataCache sharedInstance] setCacheData:fileData ForKey:weakSelf.request.URL.absoluteString];
                if (!fileValidToCache) {
                    error = [NSError errorWithDomain:CACHE_ERROR_DOMAIN_OVERLOAD_CACHE code:CACHE_ERROR_CODE_OVERLOAD_CACHE userInfo:@{NSLocalizedDescriptionKey : @"File too large to cache."}];
                }
            }
            for (CacheFileDownloadOperationCompletionBlock theCompletionBlock in self.completionBlocks) {
                theCompletionBlock(fileData, error);
            }
            [weakSelf completeOperation];
        }];
        [downloadtask resume];
    }
}

- (BOOL) isAsynchronous;{
    return YES;
}

- (BOOL)isExecuting {
    return self._executing;
}

- (BOOL)isFinished {
    return self._finished;
}

- (void)completeOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    [_session finishTasksAndInvalidate];
    _session = nil;
    _request = nil;
    _completionBlocks = nil;
    self._executing = NO;
    self._finished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end
