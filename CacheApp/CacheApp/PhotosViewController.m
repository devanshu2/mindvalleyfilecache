//
//  PhotosViewController.m
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoCollectionViewCell.h"
#import "FlickrKit.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "CacheManager.h"

#define CELL_IDENTIFIER_PHOTO @"CELL_IDENTIFIER_PHOTO"

@interface PhotosViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, retain) FKFlickrNetworkOperation *myPhotostreamOp;

@property (nonatomic, strong) NSMutableArray *photoURLs;

@property (nonatomic, assign) NSInteger currentPage;

@property (nonatomic, assign) BOOL morePages;

@end

@implementation PhotosViewController

const CGFloat cellEdge = 20.0;
const NSInteger perPageCount = PHOTO_PER_PAGE;

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.photoCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PhotoCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:CELL_IDENTIFIER_PHOTO];
    self.photoURLs = [NSMutableArray new];
    _currentPage = 0;
    _morePages = YES;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator{
    [_photoCollectionView reloadData];
}

#pragma CollectionView Methods

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return cellEdge;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return cellEdge;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(cellEdge, cellEdge, cellEdge, cellEdge);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat maxWidth = collectionView.bounds.size.width;
    NSInteger horizontalCellCount = 0;
    CGFloat sectionLeftEdge = cellEdge;
    CGFloat sectionRightEdge = cellEdge;
    CGFloat cellHorizontalSpacing = cellEdge;
    if (UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom]) {
        horizontalCellCount = 4;
    }
    else{
        horizontalCellCount = 2;
    }
    if (indexPath.row >= self.photoURLs.count){
        horizontalCellCount = 1;
    }
    CGFloat cellWidth = (maxWidth - sectionLeftEdge - sectionRightEdge - ((horizontalCellCount - 1) * cellHorizontalSpacing))/horizontalCellCount;
    return CGSizeMake(cellWidth, 135.0);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= self.photoURLs.count && !self.myPhotostreamOp) {
        _currentPage++;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AppDelegate *applicationDelegate = [[UIApplication sharedApplication] delegate];
        self.myPhotostreamOp = [[FlickrKit sharedFlickrKit] call:@"flickr.photos.search" args:@{@"user_id": applicationDelegate.flickrUserID, @"per_page": [NSString stringWithFormat:@"%ld", perPageCount], @"page": [NSString stringWithFormat:@"%ld",_currentPage]} maxCacheAge:FKDUMaxAgeNeverCache completion:^(NSDictionary *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (response) {
                    NSInteger totalImages = [[response valueForKeyPath:@"photos.total"] integerValue];
                    _morePages = (totalImages == perPageCount);
                    for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                        NSURL *url = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeSmall240 fromPhotoDictionary:photoDictionary];
                        [_photoURLs addObject:url];
                    }
                    [_photoCollectionView reloadData];
                }
                else {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            });
            self.myPhotostreamOp = nil;
        }];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_morePages) {
        return (_photoURLs.count + 1);
    }
    else{
        return _photoURLs.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER_PHOTO forIndexPath:indexPath];
    if (indexPath.row >= self.photoURLs.count){
        cell.imageView.image = nil;
        cell.midLabel.text = @"Loading date...";
    }
    else{
        NSURL *imageURL = [self.photoURLs objectAtIndex:indexPath.row];
        cell.imageView.image = nil;
        cell.midLabel.text = @"Loading Image...";
        [[CacheManager sharedInstance] dataWithURL:imageURL andCompletionBlock:^(NSData *data, NSError *error) {
            if (error) {
                NSLog(@"%@", error);
            }
            if(data){
                UIImage *image = [UIImage imageWithData:data];
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.imageView.image = image;
                    cell.midLabel.text = @"";
                });
            }
        }];
    }
    return cell;
}

@end
