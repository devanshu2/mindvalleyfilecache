//
//  PhotoCollectionViewCell.h
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *midLabel;

@end
