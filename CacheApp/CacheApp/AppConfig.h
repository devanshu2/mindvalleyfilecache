//
//  AppConfig.h
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#ifndef AppConfig_h
#define AppConfig_h

//constants
#define M_CACHE_DOWNLOAD_OPERATION_QUEUE @"M_CACHE_DOWNLOAD_OPERATION_QUEUE"

//Download operation maximum concurrency
#define QUEUE_MAX_CON_OP_COUNT 5

//Cache maximum size
#define CACHE_SIZE (50 * 1024 * 1024) //50MB

#define KEY_CACHE_DATA @"KEY_CACHE_DATA"
#define KEY_CACHE_URL @"KEY_CACHE_URL"

#define CACHE_ERROR_CODE_OVERLOAD_CACHE -44444
#define CACHE_ERROR_DOMAIN_OVERLOAD_CACHE @"CACHE_ERROR_DOMAIN_OVERLOAD_CACHE"

typedef void(^CacheFileDownloadOperationCompletionBlock)(NSData *data, NSError *error);

#define USERAUTHCALLBACKNOTIFICATION @"UserAuthCallbackNotification"

//Flickr api credentials
#define FLICKR_API_KEY @"348ea26ca45d5f9d3da7fff4822a7fd1"
#define FLICKR_API_SECRET @"471cc96b04e60f27"

//Photos per page size
#define PHOTO_PER_PAGE 8

#endif /* AppConfig_h */
