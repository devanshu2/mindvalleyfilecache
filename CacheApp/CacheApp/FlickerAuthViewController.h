//
//  FlickerAuthViewController.h
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickerAuthViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIWebView *theWebView;

@end
