//
//  CacheFileDownloadOperation.h
//  CacheApp
//
//  Created by Devanshu Saini on 03/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConfig.h"
#import "DataCache.h"


/**
 Cache file download operation class
 */
@interface CacheFileDownloadOperation : NSOperation

/**
 @brief Init with URLRequest and Completion block
 @param request
 The URLRequest of the file to download.
 @param CacheFileDownloadOperationCompletionBlock
 completionBlock to call when all done.
 @return Instance of class CacheFileDownloadOperation
 */
- (instancetype)initWithURLRequest:(NSURLRequest *)request andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock;

/**
 @brief Add completion block if need to add more
 @param CacheFileDownloadOperationCompletionBlock
 completionBlock to call when all done.
 */
- (void)addCompletionHandler:(CacheFileDownloadOperationCompletionBlock)completionBlock;

@end
