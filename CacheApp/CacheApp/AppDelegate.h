//
//  AppDelegate.h
//  CacheApp
//
//  Created by Devanshu Saini on 02/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) NSString *flickrUserID;

@end

