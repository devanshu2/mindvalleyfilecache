//
//  CacheManager.m
//  CacheApp
//
//  Created by Devanshu Saini on 02/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "CacheManager.h"

@interface CacheManager()

@property (atomic, strong) NSOperationQueue *operationQueue;

@end

@implementation CacheManager

- (instancetype)init{
    self = [super init];
    if (self) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = QUEUE_MAX_CON_OP_COUNT;
        _operationQueue.name = M_CACHE_DOWNLOAD_OPERATION_QUEUE;
        [_operationQueue addObserver:self forKeyPath:@"operations" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
}

+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    static CacheManager *theInstance = nil;
    dispatch_once(&onceToken, ^{
        theInstance = [[CacheManager alloc] init];
    });
    return theInstance;
}

- (void)dataWithURL:(NSURL *)url andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self dataWithURLRequest:request andCompletionBlock:completionBlock];
}

- (void)dataWithURLRequest:(NSURLRequest *)urlRequest andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error = nil;
        NSData *cacheData = [[DataCache sharedInstance] cacheDataForKey:urlRequest.URL.absoluteString];
        if (cacheData) {
            completionBlock(cacheData, error);
        }
        else{
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name LIKE %@", urlRequest.URL.absoluteString];
            NSArray *operations = [_operationQueue.operations filteredArrayUsingPredicate:bPredicate];
            if (operations.count) {
                for (CacheFileDownloadOperation *downloadOperation in operations) {
                    [downloadOperation addCompletionHandler:completionBlock];
                }
            }
            else{
                CacheFileDownloadOperation *downloadOperation = [[CacheFileDownloadOperation alloc] initWithURLRequest:urlRequest andCompletionBlock:completionBlock];
                [_operationQueue addOperation:downloadOperation];
            }
        }
    });
}

@end
