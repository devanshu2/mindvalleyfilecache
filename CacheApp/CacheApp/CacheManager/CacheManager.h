//
//  CacheManager.h
//  CacheApp
//
//  Created by Devanshu Saini on 02/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConfig.h"
#import "DataCache.h"
#import "CacheFileDownloadOperation.h"

/**
 Cache Manager Class to download files in form of nsdata and cache it with memory size limit
 */
@interface CacheManager : NSObject

/**
 @brief Shared instance of Cachemanager
 @return CacheManager shared instance
 */
+ (instancetype)sharedInstance;

/**
 @brief Get NSData from url, it will fetch from memory or will download
 @param url
 The URL of the file to download
 @param CacheFileDownloadOperationCompletionBlock
 The completion block.
 */
- (void)dataWithURL:(NSURL *)url andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock;


/**
 @brief Get NSData from url request wrapper, it will fetch from memory or will download
 @param urlRequest
 The URLRequest of the file to download
 @param CacheFileDownloadOperationCompletionBlock
 The completion block.
 */
- (void)dataWithURLRequest:(NSURLRequest *)urlRequest andCompletionBlock:(CacheFileDownloadOperationCompletionBlock)completionBlock;

@end
