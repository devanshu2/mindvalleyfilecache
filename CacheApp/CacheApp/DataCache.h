//
//  DataCache.h
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConfig.h"

/**
 Data Cache Class to handle cache
 */
@interface DataCache : NSObject

/**
 @brief Shared instance of class DataCache
 */
+ (instancetype)sharedInstance;

/**
 @brief Checks whether given size of data file is permissible to be stored in Cache
 @param length
 The length of data in bytes
 @return BOOL : Whether it is permissible or not
 */
- (BOOL)isValidCacheInsertWithBytes:(NSInteger)length;

/**
 @brief Provides stored cache using key
 @param key
 The key in String format
 @return NSData : The cached data
 */
- (NSData *)cacheDataForKey:(NSString *)key;

/**
 @brief Set cache data with a key value
 @param data
 The NSData to cache
 @param key
 The key in String format
 @return BOOL : Whether it is set or not
 */
- (BOOL)setCacheData:(NSData *)data ForKey:(NSString *)key;

@end
