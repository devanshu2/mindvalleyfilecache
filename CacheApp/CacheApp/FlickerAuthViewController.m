//
//  FlickerAuthViewController.m
//  CacheApp
//
//  Created by Devanshu Saini on 04/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "FlickerAuthViewController.h"
#import "FlickrKit.h"
#import "AppConfig.h"

@interface FlickerAuthViewController ()

@property (nonatomic, retain) FKDUNetworkOperation *authOperation;

@end

@implementation FlickerAuthViewController

#pragma mark - Life Cycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthenticateCallback:) name:USERAUTHCALLBACKNOTIFICATION object:nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *callbackURLString = @"cacheappdemo://auth";
    self.authOperation = [[FlickrKit sharedFlickrKit] beginAuthWithCallbackURL:[NSURL URLWithString:callbackURLString] permission:FKPermissionDelete completion:^(NSURL *flickrLoginPageURL, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:flickrLoginPageURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
                [self.theWebView loadRequest:urlRequest];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeController)];
    self.navigationItem.rightBarButtonItem = close;
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.authOperation cancel];
    [super viewWillDisappear:animated];
}

#pragma mark - Notifications
- (void)userAuthenticateCallback:(NSNotification *)notification{
    [self closeController];
}

#pragma mark - Button Actions
- (void)closeController{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Web View

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = [request URL];
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            return NO;
        }
    }
    return YES;
}

@end
