//
//  AppDelegate.m
//  CacheApp
//
//  Created by Devanshu Saini on 02/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "AppDelegate.h"
#import "CacheManager.h"
#import "FlickrKit.h"

@implementation AppDelegate

- (BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSString *scheme = [url scheme];
    if([@"cacheappdemo" isEqualToString:scheme]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:USERAUTHCALLBACKNOTIFICATION object:url userInfo:nil];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[FlickrKit sharedFlickrKit] initializeWithAPIKey:FLICKR_API_KEY sharedSecret:FLICKR_API_SECRET];
    return YES;
}

@end
