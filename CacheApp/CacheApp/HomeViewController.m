//
//  ViewController.m
//  CacheApp
//
//  Created by Devanshu Saini on 02/08/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "HomeViewController.h"
//#import "CacheManager.h"
#import "FlickrKit.h"
#import "AppDelegate.h"
#import "AppConfig.h"


@interface HomeViewController ()

@property (nonatomic, retain) FKFlickrNetworkOperation *todaysInterestingOp;
@property (nonatomic, retain) FKDUNetworkOperation *completeAuthOp;
@property (nonatomic, retain) FKDUNetworkOperation *checkAuthOp;
@property (nonatomic, retain) FKImageUploadNetworkOperation *uploadOp;

@property (weak, nonatomic) IBOutlet UIButton *photostreamButton;

@property (weak, nonatomic) IBOutlet UIButton *authButton;

@property (weak, nonatomic) IBOutlet UILabel *authLabel;


@end

@implementation HomeViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthenticateCallback:) name:USERAUTHCALLBACKNOTIFICATION object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (IBAction)photostreamButtonPressed:(id)sender {
    if ([FlickrKit sharedFlickrKit].isAuthorized) {
        [self performSegueWithIdentifier:@"photosegue" sender:nil];
    } else {
        [self authButtonPressed:nil];
    }
}

- (IBAction)authButtonPressed:(id)sender {
    if ([FlickrKit sharedFlickrKit].isAuthorized) {
        [[FlickrKit sharedFlickrKit] logout];
        [self userLoggedOut];
    } else {
        [self performSegueWithIdentifier:@"authsegue" sender:nil];
    }	
}

#pragma mark - Auth

- (void) userAuthenticateCallback:(NSNotification *)notification {
    NSURL *callbackURL = notification.object;
    self.completeAuthOp = [[FlickrKit sharedFlickrKit] completeAuthWithURL:callbackURL completion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [self userLoggedIn:userName userID:userId];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
}

- (void) userLoggedIn:(NSString *)username userID:(NSString *)userID {
    AppDelegate *applicationDelegate = [[UIApplication sharedApplication] delegate];
    applicationDelegate.flickrUserID = userID;
    [self.authButton setTitle:@"Logout" forState:UIControlStateNormal];
    self.authLabel.text = [NSString stringWithFormat:@"You are logged in as %@", username];
}

- (void) userLoggedOut {
    [self.authButton setTitle:@"Login" forState:UIControlStateNormal];
    self.authLabel.text = @"Login to flickr";
}

@end
